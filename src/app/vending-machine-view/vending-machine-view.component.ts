import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Can } from '../models/can';
import { SessionService } from '../service/session-service';

/*
Component that manages the main view of the vending machine.

Responsible to update stock level, sold cans, cach and card amounts when a user purchases a can of soda.
*/

@Component({
  selector: 'app-vending-machine-view',
  templateUrl: './vending-machine-view.component.html',
  styleUrls: ['./vending-machine-view.component.scss']
})
export class VendingMachineViewComponent implements OnInit {
 public cans : Can[] = [];
 public cashPayment : number = 0;
 public cardPayment: number = 0;

  constructor(
    private session: SessionService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.cans = JSON.parse(this.session.getCanData());
    this.cashPayment = this.session.getCashAmount();
    this.cardPayment = this.session.getCardAmount();
  }

  // The argument is the index of the array of different sodas
  public payWithCard(index : any)
  {
    this.cardPayment = this.cardPayment + this.cans[index].priceInCents;
    this.updateItem(index);
  }

  // The argument is the index of the array of different sodas
  public payWithCash(index : any)
  {
    this.cashPayment = this.cashPayment + this.cans[index].priceInCents;
    this.updateItem(index);
  
  }

  // The argument is the index of the array of different sodas
  private updateItem(index : any)
  {
    this.cans[index].numberInStock = this.cans[index].numberInStock - 1;
    this.cans[index].numberSold = this.cans[index].numberSold + 1;
    this.update();
  }

  // Refreshes the sessions storage. This would update the database 
  // in an more relastic app
  private update (){
    this.session.setCardAmount(this.cardPayment);
    this.session.setCashAmount(this.cashPayment);
    this.session.setCanData(this.cans);
  }

  public restock() {
    this.update();
    this.router.navigate([`./restock`]);
  }
}
