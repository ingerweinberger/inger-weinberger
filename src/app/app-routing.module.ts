import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RestockComponent } from './restock/restock.component';
import { VendingMachineViewComponent } from './vending-machine-view/vending-machine-view.component';

const routes: Routes = [
  { path: '', component: VendingMachineViewComponent },
  { path: 'restock', component: RestockComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
