import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Can } from '../models/can';
import { SessionService } from '../service/session-service';


/*
Component that manages the restock view of the vending machine.

Responsible to reset sold cans, cash and card amounts when the vending machine has been restocked.
Also manages to add a user selectable amount of cans to the stock level.
*/

@Component({
  selector: 'app-restock',
  templateUrl: './restock.component.html',
  styleUrls: ['./restock.component.scss']
})

export class RestockComponent implements OnInit {
  public cans : Can[] = [];
  public cashPayment : number = 0;
  public cardPayment: number = 0;
  public newStock: string[] = [];
  public totalSold: number = 0;
  public totalInStock: number = 0;
  
  constructor(
    private session: SessionService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.cans = JSON.parse(this.session.getCanData());
    this.cashPayment = this.session.getCashAmount();
    this.cardPayment = this.session.getCardAmount();
    this.totalSold = this.cans.filter(item => item.numberSold).reduce((sum, current) => sum + current.numberSold, 0);
    this.totalInStock = this.cans.filter(item => item.numberInStock).reduce((sum, current) => sum + current.numberInStock, 0);
  }


  /*
  Loops through the array of cans to update the number of cans in stock with the number in the input field for each respective type of soda.
  Also resets total sold, cash and card amount.
  Navigates back to the main vending view when done.
  */
  public restock()
  {
    for(var i= 0; i< this.newStock.length; i++) {

       this.cans[i].numberInStock = !isNaN(parseInt(this.newStock[i])) ? this.cans[i].numberInStock + parseInt(this.newStock[i]) 
                                    : this.cans[i].numberInStock;
    }
    this.totalInStock = this.cans.filter(item => item.numberInStock).reduce((sum, current) => sum + current.numberInStock, 0);
    this.cans.filter(x => x.numberSold).forEach(x=> x.numberSold = 0);
    this.totalSold = 0;
    this.newStock = [];
    this.resetCardAmount();
    this.resetCashAmount();
    this.session.setCanData(this.cans);
    this.router.navigate(['']);
  }

  public resetCardAmount() {
    this.cardPayment = 0;
    this.session.setCardAmount(this.cardPayment);
  }
  
  public resetCashAmount() {
    this.cashPayment = 0;
    this.session.setCashAmount(this.cashPayment);
  }
}
