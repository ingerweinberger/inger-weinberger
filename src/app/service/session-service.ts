import { Injectable } from "@angular/core";
import { Can } from "../models/can";

@Injectable({
    providedIn: 'root'
  })

/*
Manages all the session variables. 

Getter and setters for all the session variables.
*/

export class SessionService {

    setCanData(value: Can[]) {        
        sessionStorage.setItem('cans', JSON.stringify(value));
    }

    getCanData() : string {
        return sessionStorage.getItem('cans') || '';
    }

    setCardAmount(value : number) {
        sessionStorage.setItem('cardAmount', value.toString()); 
    }

    getCardAmount(): number {
        return parseInt(sessionStorage.getItem('cardAmount') || '0');
    }

    setCashAmount(value : number) {
        sessionStorage.setItem('cashAmount', value.toString()); 
    }

    getCashAmount(): number {
        return parseInt(sessionStorage.getItem('cashAmount') || '0');
    }

    setAmountSold(value : number) {
        sessionStorage.setItem('amountSold', value.toString()); 
    }

    getAmountSold(): number {
        return parseInt(sessionStorage.getItem('amountSold') || '0');
    }
}
