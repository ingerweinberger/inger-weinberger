import { Component, OnInit} from '@angular/core';
import { CansSearchResponse } from './models/mock-db/cans-search-response';
import { SessionService } from './service/session-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private session: SessionService,
  ) { }

  ngOnInit(): void {
    this.session.setCanData(CansSearchResponse.cans);
    this.session.setCardAmount(CansSearchResponse.cardAmount); 
    this.session.setCashAmount(CansSearchResponse.cashAmount);
  }
  public title = 'vending-machine';
}
