/*
Defines the data of a Can 
*/
export interface Can {
    name: string;
    priceInCents: number;
    numberInStock: number;
    numberSold: number;
}
