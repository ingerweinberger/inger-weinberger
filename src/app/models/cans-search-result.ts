import { Can } from "./can";


/*
Defines the data return from the "database"
*/
export interface CansSearchResult {
    cardAmount: number;
    cashAmount: number;
    cans: Can[];
}