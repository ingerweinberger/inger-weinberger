import { CansSearchResult } from "../cans-search-result";


/*
Simulates the response from the database. The data is the kept in the session storage.
*/
export const CansSearchResponse: CansSearchResult = {
    cardAmount: 0,
    cashAmount: 0,
    cans: [
        {
            name: 'Strawberry',
            priceInCents: 150,
            numberInStock: 10,
            numberSold: 0
        },
        {
            name: 'Raspberry',
            priceInCents: 150,
            numberInStock: 8,
            numberSold: 0
        },
        {
            name: 'Mango',
            priceInCents: 150,
            numberInStock: 5,
            numberSold: 0
        },
        {
            name: 'Lemon',
            priceInCents: 150,
            numberInStock: 7,
            numberSold: 0
        },
        {
            name: 'Peach',
            priceInCents: 150,
            numberInStock: 7,
            numberSold: 0
        },
        {
            name: 'Ginger',
            priceInCents: 250,
            numberInStock: 10,
            numberSold: 0
        },
        {
            name: 'Cola',
            priceInCents: 250,
            numberInStock: 15,
            numberSold: 0
        },
        {
            name: 'Sprite',
            priceInCents: 250,
            numberInStock: 10,
            numberSold: 0
        },
        {
            name: 'Fanta',
            priceInCents: 250,
            numberInStock: 10,
            numberSold: 0
        },
        {
            name: 'Pepsi',
            priceInCents: 250,
            numberInStock: 10,
            numberSold: 0
        }
    ]

}