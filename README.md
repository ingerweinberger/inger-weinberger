## Readme for code test "Vending machine"

# Assumptions
* No database. All data is only kept in the session store.
* Money is stored as cents to avoid problems with doubles or decimals.
* Cans are stored in an array of cans that serves both to populate the main vending machine UI and to 
keep the data about the current state of stock, price, sold, and name of the soda.

Please run 'npm install' after cloning this repo.
# VendingMachine

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
